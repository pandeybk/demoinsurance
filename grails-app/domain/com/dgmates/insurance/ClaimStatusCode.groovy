package com.dgmates.insurance

class ClaimStatusCode {

    String statusDescription

    static constraints = {
    }

    static mapping = {
        id column: 'status_code'
        version false
    }
}
