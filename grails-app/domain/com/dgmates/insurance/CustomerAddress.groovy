package com.dgmates.insurance

class CustomerAddress {

    Date dateAddressFrom
    Date dateAddressTo

    static belongsTo = [customer:Customer, address:Address]

    static constraints = {
    }

    static mapping = {
        id generator: 'assigned', name: 'dateAddressFrom'
        version false
    }
}
