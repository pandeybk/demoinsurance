package com.dgmates.insurance

class Life {

    Policy policy
    RefInsuranceType insuranceTypeCode
    Broker broker

    Date policyStartDate
    Date policyRenewalDate
    Float premiumPayable
    String otherPolicyDetails
    String occupationCode
    String lifeExpectancy


    static constraints = {
    }

    static mapping = {
        version false
    }
}
