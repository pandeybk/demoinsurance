package com.dgmates.insurance

class CustomerPolicy {

    String customerPolicyDetails

    static belongsTo = [customer:Customer, policy:Policy]

    static constraints = {
    }

    static mapping = {
        id column:'customer_policy_id'
        version false
    }
}
