package com.dgmates.insurance

class InsuranceCompany {

    String companyName
    String otherDetails

    static belongsTo = [address:Address]

    static constraints = {
    }

    static mapping = {
        id column:'company_id'
        version false
    }
}
