package com.dgmates.insurance

class Household {

    Policy policy
    RefInsuranceType insuranceTypeCode
    Broker broker

    Date policyStartDate
    Date policyRenewalDate
    Float premiumPayable
    String otherPolicyDetails

    String ageOfProperty

    static constraints = {

    }
    static mapping = {
        version false
    }
}
