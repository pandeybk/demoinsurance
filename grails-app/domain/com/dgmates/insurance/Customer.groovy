package com.dgmates.insurance

class Customer {

    String customerDetails

    static constraints = {
    }

    static mapping = {
        id column:'customer_id'
        version false
    }
}
