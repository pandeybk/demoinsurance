package com.dgmates.insurance

class Policy {

    Date policyStartDate
    Date policyRenewalDate

    String premiumPayable
    String otherPolicyDetails

    Broker broker
    RefInsuranceType insuranceTypeCode

    static constraints = {
    }

    static mapping = {
        id column:'policy_id'
        version false
    }
}
