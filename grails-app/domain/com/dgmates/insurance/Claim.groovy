package com.dgmates.insurance

class Claim {

    Date dateOfClaim
    Float amountOfClaim
    String detailsOfClaims

    static belongsTo = [customerPolicy:CustomerPolicy, statusCode:ClaimStatusCode, outcomeCode:ClaimOutcome]

    static constraints = {
    }

    static mapping = {
        id column: 'claim_id'
        version false
    }
}
