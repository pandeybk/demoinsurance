package com.dgmates.insurance

class Motor {

    Policy policy
    RefInsuranceType insuranceTypeCode
    Broker broker

    Date policyStartDate
    Date policyRenewalDate
    Float premiumPayable
    String otherPolicyDetails
    String vehicleDetails

    static constraints = {

    }

    static mapping = {
        version false
    }
}
