package com.dgmates.insurance

class ClaimOutcome {

    String outcomeDescription

    static constraints = {
    }

    static mapping = {
        id column:'outcome_code'
        version false
    }
}
