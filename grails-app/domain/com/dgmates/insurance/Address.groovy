package com.dgmates.insurance

class Address {

    String line1NumberBuilding
    String line2NumberStreet
    String line3AreaSuburb
    String townCity
    String stateCountry
    String country
    String postalZipcode

    static hasMany = [insuranceCompany:InsuranceCompany, broker:Broker]

    static constraints = {
    }

    static mapping = {
        id column:'address_id'
        version false
    }
}
