package com.dgmates.insurance

class HouseholdItem {

    Float itemValue
    String itemDescription

    static belongsTo = [household:Household]

    static constraints = {
    }

    static mapping = {
        id column:'item_id'
        version false
    }
}
