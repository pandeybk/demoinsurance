package com.dgmates.insurance

class Broker {

    String brokerName
    String otherBrokerDetails

    static belongsTo = [address:Address]

    static constraints = {
    }

    static mapping = {
        id column:'broker_id'
        version false
    }
}
