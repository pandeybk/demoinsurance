package com.dgmates.insurance

class RefInsuranceType {

    String insuranceTypeDescription

    static constraints = {
    }

    static mapping = {
        id column:'insurance_type_code'
        version false
    }
}
